# Amstrad CPC Locomotive BASIC port of Scott Adams Grand Adventures (SAGA)

A port to the Amstrad CPC Locomotive BASIC of the classic TRS-80 BASIC version of Scott Adams's text adventures games Adventureland, as published in [SoftSide magazine](https://archive.org/details/softside-magazine-22/page/n35/mode/2up) Pirate Adventure, as published in [BYTE magazine](https://archive.org/details/byte-magazine-1980-12/page/n193/mode/1up) in 1980. 

The work published here is inspired and based on the work done by ahope1 for the BBC micro and documented in [scott-adamss-type-in-pirate-adventure-byte-1980](https://ahopeful.wordpress.com/2020/08/25/scott-adamss-type-in-pirate-adventure-byte-1980/) and [digging-up-adventureland-scott-adams-1980](https://ahopeful.wordpress.com/2020/09/13/digging-up-adventureland-scott-adams-1980/). Thanks for your great work!


## The game files

**game.bas** — It is the interpreter code written in BASIC as it appeared in BYTE magazine. The code is taking from [ahope1 github repository](https://github.com/ahope1/Beeb-Pirate-Adventure). Very small modifications done to adapt the code to Locomtive BASIC implementation, it adds the selection of the game and prints nicely the text in the Amstrad CPC screen mode 2 (80 columns).

**adv01.bas** — Adventureland game data file generator, when run it generates adv01.dat as found in assets folder.

**adv01.bas** — Pirate Adventure game data file generator, when run it generates adv02.dat as found in assets folder.

## Requirements

 - GNU Make (others may work)
 - a POSIX compatible environment

## Building

To build the DSK image, run:

```    
    make
```

After a successful build, the dsk file saga.dsk can be used in an emulator.

Happy adventuring!!

![](assets/pirates.png)

## TODO

Finish the ScottFree port to Amstrad CPC which includes accompanying images to the text of Adventureland....

