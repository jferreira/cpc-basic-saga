TARGET=saga
export PATH:=tools:$(PATH)

SRCS = $(wildcard *.bas)
DATS = $(wildcard assets/*.dat)

.PHONY: all clean

all:
	make -C tools
	make $(TARGET).dsk

$(TARGET).dsk: $(SRCS) $(DATS)
	rm -f $@
	iDSK $@ -n
	$(foreach SRC,$(SRCS), iDSK $@ -i $(SRC) -t 0;)
	$(foreach DAT,$(DATS), iDSK $@ -i $(DAT) -t 0;)

clean:
	rm -f $(TARGET).dsk

